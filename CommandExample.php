<?php
/**
 * Created by PhpStorm.
 * User: Edan
 * Date: 18/08/2016
 * Time: 21:38
 */

include('library/BotStartAPI.inc.php');
$bot = new \BotStart\Bot();
$cmd1 = new \BotStart\Command($bot, "Hello World", "!hello", new \BotStart\SendMessage("Hello World"), \BotStart\BotActionScope::ALL);
$cmd2 = new \BotStart\Command($bot, "Hello, Again", "!bothermemore", new \BotStart\SendMessage("Sup, brother?", \BotStart\BotActionScope::SENDER));