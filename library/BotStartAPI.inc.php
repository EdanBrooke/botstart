<?php
/**
 * Created by PhpStorm.
 * User: Edan
 * Date: 18/08/2016
 * Time: 21:35
 */
set_include_path(realpath(dirname(__FILE__)) . '/');
spl_autoload_extensions('.class.php');
spl_autoload_register(function($class) {
    include get_include_path().str_replace('\\', '/', $class).'.class.php';
});