<?php
/**
 * Created by PhpStorm.
 * User: Edan
 * Date: 18/08/2016
 * Time: 22:30
 */

namespace BotStart;
use WebSocket;

class Listener
{
    private $gateway;
    private $socket;
    public function __construct($clientId, $token)
    {
        $ch = \curl_init();
        curl_setopt($ch, CURLOPT_URL,'https://discordapp.com/api/gateway');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/json",
            'Authorization: Bot '.$token,
        ));
        $data = curl_exec($ch);
        if (curl_errno($ch)) {
            echo "Reg error " . curl_error($ch);
        }
        $gateway = json_decode($data, true);
        if(isset($gateway["url"])){
            $this->gateway = str_replace("wss://","",$gateway["url"]);
            $this->socket = new \WebSocket\Client("ws://gateway.discord.gg/");
            $this->socket->send("Hello");
            echo $this->socket->receive();
        }
        echo('<br />');
    }
}