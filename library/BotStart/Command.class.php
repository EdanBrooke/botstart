<?php
/**
 * Created by PhpStorm.
 * User: Edan
 * Date: 18/08/2016
 * Time: 21:35
 */

namespace BotStart;


class Command
{
    protected $bot;
    protected $name;
    protected $command;
    protected $listener; // Note that a command listener is different from a bot listener.
    protected $action;
    protected $usage;
    public function __construct($bot, $name, $command, $action, $usage = null)
    {
        $this->bot = $bot;
        $this->name = $name;
        $this->command = $command;
        $this->action = $action;
        $this->usage = $usage;
        $bot->registerCommand($this);
        echo 'Command <b>'.$this->name.'</b> registered with command <i>'.$command.'</i> on bot <b>'.$bot->getClientId().'</b>.<br />';
    }
}