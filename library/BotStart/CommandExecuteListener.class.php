<?php
/**
 * Created by PhpStorm.
 * User: Edan
 * Date: 18/08/2016
 * Time: 23:25
 */

namespace BotStart;


class CommandExecuteListener
{
    protected $bot;
    protected $command;
    public function __construct($bot, $command){
        $this->bot = $bot;
        $this->command = $command;
    }
}