<?php
/**
 * Created by PhpStorm.
 * User: Edan
 * Date: 18/08/2016
 * Time: 21:33
 */

namespace BotStart;


class Bot
{
    private $clientId;
    private $token;
    private $commands;
    private $listener;
    public function __construct($clientId, $token)
    {
        $this->clientId = $clientId;
        $this->token = $token;
        $this->commands = array();
        $this->listener = new Listener($clientId, $token);
        echo 'Bot started on Client ID <b>'.$this->clientId.'</b>.<br />';
    }
    public function getClientId(){
        return $this->clientId;
    }
    public function registerCommand($command){
        array_push($this->commands, $command);
    }
}