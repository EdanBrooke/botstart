<?php
/**
 * Created by PhpStorm.
 * User: Edan
 * Date: 18/08/2016
 * Time: 23:38
 */

namespace BotStart;


abstract class BotActionScope
{
    const ALL = 0;
    const SENDER = 1;
}